﻿/**
* Copyright 2015 IBM Corp. All Rights Reserved.
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*
*/
#pragma warning disable 0649

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using IBM.Watson.SpeechToText.V1;
using IBM.Cloud.SDK;
using IBM.Cloud.SDK.Authentication;
using IBM.Cloud.SDK.Authentication.Iam;
using IBM.Cloud.SDK.Utilities;
using IBM.Cloud.SDK.DataTypes;

using IBM.Watson.TextToSpeech.V1;
using IBM.Watson.ToneAnalyzer.V3;
using System.Text.RegularExpressions;


namespace IBM.Watsson.Examples
{
    public class ExampleStreaming : MonoBehaviour
    {
     
        #region PLEASE SET THESE VARIABLES IN THE INSPECTOR
        [Space(10)]
        [Tooltip("The service URL (optional). This defaults to \"https://stream.watsonplatform.net/speech-to-text/api\"")]
        [SerializeField]
        private string _serviceUrl;
        [Tooltip("Text field to display the results of streaming.")]
        public Text ResultsField;
        [Header("IAM Authentication")]
        [Tooltip("The IAM apikey.")]
        [SerializeField]
        private string _iamApikey;

        [Header("Parameters")]
        // https://www.ibm.com/watson/developercloud/speech-to-text/api/v1/curl.html?curl#get-model
        [Tooltip("The Model to use. This defaults to en-US_BroadbandModel")]
        [SerializeField]
        private string _recognizeModel;


        [Space(10)]
        [Tooltip("The service URL (optional). This defaults to \"https://stream.watsonplatform.net/text-to-speech/api\"")]
        [SerializeField]
        private string _serviceUrl1;
       
        [Header("IAM Authentication")]
        [Tooltip("The IAM apikey.")]
        [SerializeField]
        private string _iamApikey1;

        [Header("Parameters")]
        // https://www.ibm.com/watson/developercloud/text-to-speech/api/v3/curl.html?curl#get-model
        [Tooltip("The Model to use. This defaults to en-US_BroadbandModel")]
        [SerializeField]

        




        #endregion



        private int _recordingRoutine = 0;
        private string _microphoneID = null;
        private AudioClip _recording = null;
        private int _recordingBufferSize = 1;
        private int _recordingHZ = 22050;

        private SpeechToTextService _service;
    
        public AudioSource _audio;
        public AudioClip Ans1;
        public AudioClip Ans2;
        public AudioClip Ans3;
        public AudioClip Ans4;
        public AudioClip Ans5;
        public AudioClip Ans6;
        public AudioClip Ans7;
        public AudioClip Ans8;
        public AudioClip Ans9;
        public AudioClip Ans10;
        public AudioClip Ans11;
        public AudioClip Ans12;
        public AudioClip Ans13;
        public AudioClip Ans14;
        public AudioClip Ans15;
        public AudioClip Ans16;
        public AudioClip Ans17;
        public AudioClip Ans18;
        public AudioClip Ans19;
        public AudioClip Ans20;
        public AudioClip Ans21;
        public AudioClip Ans22;
        public AudioClip Ans23;
        public AudioClip Ans24;
        public AudioClip Ans25;
        public AudioClip Ans26;



        void Start()
        {
            _audio = gameObject.GetComponent<AudioSource>();
            LogSystem.InstallDefaultReactors();
            Runnable.Run(CreateService());
           // StartCoroutine(DownloadTheAudio());
           // _audio.clip = Ans1;
           // _audio.Play();

        }


        // edited


        IEnumerator DownloadTheAudio()
        {
           string url = "https://translate.google.com/translate_tts?ie=UTF-8&total=1&idx=0&textlen=32&client=tw-ob&q="+ResultsField.text+"&tl=En-gb";
           // string url = "https://translate.google.com/translate_tts?ie=UTF-8&total=1&idx=0&textlen=32&client=tw-ob&q=" + text.text + "&tl=En-gb";
            WWW www = new WWW(url);
            yield return www;

          //  _audio.clip = www.GetAudioClip(false, true, AudioType.MPEG);
          //  _audio.Play();
           

        }

        private IEnumerator CreateService()
        {
            if (string.IsNullOrEmpty(_iamApikey))
            {
                throw new IBMException("Plesae provide IAM ApiKey for the service.");
            }

            IamAuthenticator authenticator = new IamAuthenticator(apikey: _iamApikey);

            //  Wait for tokendata
            while (!authenticator.CanAuthenticate())
                yield return null;

            _service = new SpeechToTextService(authenticator);
            if (!string.IsNullOrEmpty(_serviceUrl))
            {
                _service.SetServiceUrl(_serviceUrl);
            }
            _service.StreamMultipart = true;

           // TTS credentials





            Active = true;
            StartRecording();
           
        }

        public bool Active
        {
            get { return _service.IsListening; }
            set
            {
                if (value && !_service.IsListening)
                {
                    _service.RecognizeModel = (string.IsNullOrEmpty(_recognizeModel) ? "en-US_BroadbandModel" : _recognizeModel);
                    _service.DetectSilence = true;
                    _service.EnableWordConfidence = true;
                    _service.EnableTimestamps = true;
                    _service.SilenceThreshold = 0.01f;
                    _service.MaxAlternatives = 1;
                    _service.EnableInterimResults = true;
                    _service.OnError = OnError;
                    _service.InactivityTimeout = -1;
                    _service.ProfanityFilter = false;
                    _service.SmartFormatting = true;
                    _service.SpeakerLabels = false;
                    _service.WordAlternativesThreshold = null;
                    _service.StartListening(OnRecognize, OnRecognizeSpeaker);
                }
                else if (!value && _service.IsListening)
                {
                    _service.StopListening();
                }
            }
        }

        private void StartRecording()
        {
            if (_recordingRoutine == 0)
            {
                UnityObjectUtil.StartDestroyQueue();
                _recordingRoutine = Runnable.Run(RecordingHandler());
            }
        }

        private void StopRecording()
        {
            if (_recordingRoutine != 0)
            {
                Microphone.End(_microphoneID);
                Runnable.Stop(_recordingRoutine);
                _recordingRoutine = 0;
            }
        }

        private void OnError(string error)
        {
            Active = false;

            Log.Debug("ExampleStreaming.OnError()", "Error! {0}", error);
        }

        private IEnumerator RecordingHandler()
        {
            Log.Debug("ExampleStreaming.RecordingHandler()", "devices: {0}", Microphone.devices);
            _recording = Microphone.Start(_microphoneID, true, _recordingBufferSize, _recordingHZ);
            yield return null;      // let _recordingRoutine get set..

            if (_recording == null)
            {
                StopRecording();
                yield break;
            }

            bool bFirstBlock = true;
            int midPoint = _recording.samples / 2;
            float[] samples = null;

            while (_recordingRoutine != 0 && _recording != null)
            {
                int writePos = Microphone.GetPosition(_microphoneID);
                if (writePos > _recording.samples || !Microphone.IsRecording(_microphoneID))
                {
                    Log.Error("ExampleStreaming.RecordingHandler()", "Microphone disconnected.");

                    StopRecording();
                    yield break;
                }

                if ((bFirstBlock && writePos >= midPoint)
                  || (!bFirstBlock && writePos < midPoint))
                {
                    // front block is recorded, make a RecordClip and pass it onto our callback.
                    samples = new float[midPoint];
                    _recording.GetData(samples, bFirstBlock ? 0 : midPoint);

                    AudioData record = new AudioData();
                    record.MaxLevel = Mathf.Max(Mathf.Abs(Mathf.Min(samples)), Mathf.Max(samples));
                    record.Clip = AudioClip.Create("Recording", midPoint, _recording.channels, _recordingHZ, false);
                    record.Clip.SetData(samples, 0);

                    _service.OnListen(record);

                    bFirstBlock = !bFirstBlock;
                }
                else
                {
                    // calculate the number of samples remaining until we ready for a block of audio, 
                    // and wait that amount of time it will take to record.
                    int remaining = bFirstBlock ? (midPoint - writePos) : (_recording.samples - writePos);
                    float timeRemaining = (float)remaining / (float)_recordingHZ;

                    yield return new WaitForSeconds(timeRemaining);
                }
            }
            yield break;
        }

        private void OnRecognize(SpeechRecognitionEvent result)
        {
            if (result != null && result.results.Length > 0)
            {
                foreach (var res in result.results)
                {
                    foreach (var alt in res.alternatives)
                    {
                        string text = string.Format("{0} ({1}, {2:0.00})\n", alt.transcript, res.final ? "Final" : "Interim", alt.confidence);
                        Log.Debug("ExampleStreaming.OnRecognize()", text);
                        ResultsField.text = text;
                                                    if (alt.transcript.Contains("what is your name" ) && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                                                        {   text = "My name is pebo";
                                                            ResultsField.text = text;
                                                            //StartCoroutine(DownloadTheAudio());
                            _audio.clip = Ans1;
                            _audio.Play();
                           

                        }
                                                    if ((alt.transcript.Contains("your name ") || alt.transcript.Contains("name")) && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                                                        {
                                                            text = "My name is pebo";
                                                            ResultsField.text = text;
                                                          //  StartCoroutine(DownloadTheAudio());
                            _audio.clip = Ans1;
                            _audio.Play();

                        }
                                                     if (alt.transcript.Contains("what name") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                                                        {
                                                            text = "My name is pebo";
                                                            ResultsField.text = text;
                                                          //  StartCoroutine(DownloadTheAudio());
                            _audio.clip = Ans1;
                            _audio.Play();

                        }
                                                    if (alt.transcript.Contains("tell me about you") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                                                        {
                                                              text = "I am a pebo, and i can provide assistant during the game. I am created by Indian Institute of Science Bangalore";
                                                              ResultsField.text = text;
                                                           //   StartCoroutine(DownloadTheAudio());
                            _audio.clip = Ans2;
                            _audio.Play();
                        }
                                                    if (alt.transcript.Contains("about you") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                                                        {
                                                        text = "I am a pebo, and i can provide assistant during the game. I am created by Indian Institute of Science Bangalore";
                                                        ResultsField.text = text;
                                                       // StartCoroutine(DownloadTheAudio());
                            _audio.clip = Ans2;
                            _audio.Play();
                        }
                                                    if (alt.transcript.Contains("who are you") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                                                       {
                                                        text = "I am a pebo, and i can provide assistant during the game. I am created by Indian Institute of Science Bangalore";
                                                        ResultsField.text = text;
                                                      //  StartCoroutine(DownloadTheAudio());
                            _audio.clip = Ans2;
                            _audio.Play();
                        }
                                                    if (alt.transcript.Contains("hi") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                                                         {
                                                              text = "hello how are you";
                                                              ResultsField.text = text;
                                                          //    StartCoroutine(DownloadTheAudio());
                            _audio.clip = Ans3;
                            _audio.Play();
                        }
                        if (alt.transcript.Contains("I am not good") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "Okay, that not good, how I can help you";
                            ResultsField.text = text;
                         //   StartCoroutine(DownloadTheAudio());
                            _audio.clip = Ans4;
                            _audio.Play();
                        }
                        if (alt.transcript.Contains("Not good") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "Okay, thats not good, how I can help you";
                            ResultsField.text = text;
                            //   StartCoroutine(DownloadTheAudio());
                            _audio.clip = Ans4;
                            _audio.Play();
                        }
                        if (alt.transcript.Contains("hello") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                                                         {
                                                            text = "Hey, how are you";
                                                            ResultsField.text = text;
                            //   StartCoroutine(DownloadTheAudio());
                            _audio.clip = Ans5;
                            _audio.Play();
                        }
                        
                        if (alt.transcript.Contains("one") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "நான் நன்றாக இருக்கிறேன்";
                            ResultsField.text = text;
                          //  StartCoroutine(DownloadTheAudio());
                        }
                        if (alt.transcript.Contains("how are you") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                                                         {
                                                            text = "I am awesome";
                                                            ResultsField.text = text;
                                                          //  StartCoroutine(DownloadTheAudio());
                            _audio.clip = Ans6;
                            _audio.Play();
                        }
                                                    if (alt.transcript.Contains("okay") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                                                           {
                                                              text = "Would you like to ask a question. I would like to help you.";
                                                              ResultsField.text = text;
                                                            //  StartCoroutine(DownloadTheAudio());
                            _audio.clip = Ans7;
                            _audio.Play();
                        }
                                                    if (alt.transcript.Contains("ok") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                                                        {
                                                            text = "Would you like to ask a question. I would like to help you.";
                                                            ResultsField.text = text;
                                                           // StartCoroutine(DownloadTheAudio());
                            _audio.clip = Ans7;
                            _audio.Play();
                        }
                        if (alt.transcript.Contains("what is friction force") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "To stop a moving object, a force must act in the opposite direction to the direction of motion. For instance, if you push your book across your desk, the book will move.";
                            ResultsField.text = text;
                          //  StartCoroutine(DownloadTheAudio());
                            _audio.clip = Ans8;
                            _audio.Play();
                        }
                        if (alt.transcript.Contains("fiction") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "To stop a moving object, a force must act in the opposite direction to the direction of motion. For instance, if you push your book across your desk, the book will move. The force of the push moves the book. As the book slides across the desk, it slows down and stops moving. The force that opposes the motion of an object is called friction.";
                            ResultsField.text = text;
                          //  StartCoroutine(DownloadTheAudio());
                            _audio.clip = Ans8;
                            _audio.Play();
                        }
                        if (alt.transcript.Contains("friction force") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "To stop a moving object, a force must act in the opposite direction to the direction of motion. For instance, if you push your book across your desk, the book will move. The force of the push moves the book. As the book slides across the desk, it slows down and stops moving. The force that opposes the motion of an object is called friction.";
                            ResultsField.text = text;
                           // StartCoroutine(DownloadTheAudio());
                            _audio.clip = Ans8;
                            _audio.Play();
                        }
                        if (alt.transcript.Contains("force") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "To stop a moving object, a force must act in the opposite direction to the direction of motion. For instance, if you push your book across your desk, the book will move. The force of the push moves the book. As the book slides across the desk, it slows down and stops moving. The force that opposes the motion of an object is called friction.";
                            ResultsField.text = text;
                            //StartCoroutine(DownloadTheAudio());
                            _audio.clip = Ans8;
                            _audio.Play();
                        }
                        if (alt.transcript.Contains("type of fiction force") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "There are two type of friction force. 1. Static Friction   2. Kinetic Friction";
                            ResultsField.text = text;
                            //StartCoroutine(DownloadTheAudio());
                            _audio.clip = Ans9;
                            _audio.Play();
                        }
                        if (alt.transcript.Contains("type force") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "There are two type of friction force. 1. Static Friction   2. Kinetic Friction";
                            ResultsField.text = text;
                           // StartCoroutine(DownloadTheAudio());
                            _audio.clip = Ans9;
                            _audio.Play();
                        }
                        if (alt.transcript.Contains("what is static friction force") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "Static friction is the friction that exists between a stationary object and the surface on which it's resting.";
                            ResultsField.text = text;
                           // StartCoroutine(DownloadTheAudio());
                            _audio.clip = Ans10;
                            _audio.Play();
                        }
                        if (alt.transcript.Contains("static friction force") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "Static friction is the friction that exists between a stationary object and the surface on which it's resting.";
                            ResultsField.text = text;
                            // StartCoroutine(DownloadTheAudio());
                            _audio.clip = Ans10;
                            _audio.Play();
                        }
                        if (alt.transcript.Contains("static friction") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "Static friction is the friction that exists between a stationary object and the surface on which it's resting.";
                            ResultsField.text = text;
                            // StartCoroutine(DownloadTheAudio());
                            _audio.clip = Ans10;
                            _audio.Play();
                        }
                        if (alt.transcript.Contains("what is static force") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "static friction is the friction that exists between a stationary object and the surface on which it's resting.";
                            ResultsField.text = text;
                           // StartCoroutine(DownloadTheAudio());
                            _audio.clip = Ans10;
                            _audio.Play();
                        }
                        if (alt.transcript.Contains("static force") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "Static friction is the friction that exists between a stationary object and the surface on which it's resting.";
                            ResultsField.text = text;
                         //   StartCoroutine(DownloadTheAudio());
                            _audio.clip = Ans10;
                            _audio.Play();
                        }
                        if (alt.transcript.Contains("what is kinetic friction force") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "Kinetic friction is a force that acts between moving surfaces.";
                            ResultsField.text = text;
                          //  StartCoroutine(DownloadTheAudio());
                            _audio.clip = Ans11;
                            _audio.Play();
                        }
                        if (alt.transcript.Contains("kinetic friction force") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "Kinetic friction is a force that acts between moving surfaces.";
                            ResultsField.text = text;
                            //  StartCoroutine(DownloadTheAudio());
                            _audio.clip = Ans11;
                            _audio.Play();
                        }
                        if (alt.transcript.Contains("kinetic friction") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "Kinetic friction is a force that acts between moving surfaces.";
                            ResultsField.text = text;
                            //  StartCoroutine(DownloadTheAudio());
                            _audio.clip = Ans11;
                            _audio.Play();
                        }
                        if (alt.transcript.Contains("kinetic") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "Kinetic friction is a force that acts between moving surfaces.";
                            ResultsField.text = text;
                           // StartCoroutine(DownloadTheAudio());
                            _audio.clip = Ans12;
                            _audio.Play();
                        }
                        if (alt.transcript.Contains("what is Kinetic force") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "Kinetic friction is a force that acts between moving surfaces.";
                            ResultsField.text = text;
                           // StartCoroutine(DownloadTheAudio());
                            _audio.clip = Ans12;
                            _audio.Play();
                        }
                        if (alt.transcript.Contains("what is kinetic force") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "Kinetic friction is a force that acts between moving surfaces.";
                            ResultsField.text = text;
                            // StartCoroutine(DownloadTheAudio());
                            _audio.clip = Ans12;
                            _audio.Play();
                        }

                        /***************
                         new question ans 
                         *********** */


                        if (alt.transcript.Contains("what is projectile motion") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "Projectile motion is a form of motion experienced by an object or particle that is thrown near the Earth's surface and moves along a curved path under the action of gravity only.";
                            ResultsField.text = text;
                           // StartCoroutine(DownloadTheAudio());
                            _audio.clip = Ans12;
                            _audio.Play();
                        }
                        if (alt.transcript.Contains("projectile motion") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "Projectile motion is a form of motion experienced by an object or particle that is thrown near the Earth's surface and moves along a curved path under the action of gravity only.";
                            ResultsField.text = text;
                            // StartCoroutine(DownloadTheAudio());
                            _audio.clip = Ans12;
                            _audio.Play();
                        }
                        if (alt.transcript.Contains("projectile") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "Projectile motion is a form of motion experienced by an object or particle that is thrown near the Earth's surface and moves along a curved path under the action of gravity only.";
                            ResultsField.text = text;
                            // StartCoroutine(DownloadTheAudio());
                            _audio.clip = Ans12;
                            _audio.Play();
                        }
                        if (alt.transcript.Contains("what is angle of projection.") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "The angle between the initial velocity of a body from a horizontal plane through which the body is thrown, is known as angle of projection.";
                            ResultsField.text = text;
                           // StartCoroutine(DownloadTheAudio());
                            _audio.clip = Ans13;
                            _audio.Play();
                        }
                        if (alt.transcript.Contains("angle of projection.") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "The angle between the initial velocity of a body from a horizontal plane through which the body is thrown, is known as angle of projection.";
                            ResultsField.text = text;
                            // StartCoroutine(DownloadTheAudio());
                            _audio.clip = Ans13;
                            _audio.Play();
                        }
                        if (alt.transcript.Contains("what is range of projectile motion") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "The horizontal range of a projectile is the distance along the horizontal plane it would travel, before reaching the same vertical position as it started from.";
                            ResultsField.text = text;
                          //  StartCoroutine(DownloadTheAudio());
                            _audio.clip = Ans14;
                            _audio.Play();
                        }
                        if (alt.transcript.Contains("range of projectile motion") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "The horizontal range of a projectile is the distance along the horizontal plane it would travel, before reaching the same vertical position as it started from.";
                            ResultsField.text = text;
                            //  StartCoroutine(DownloadTheAudio());
                            _audio.clip = Ans14;
                            _audio.Play();
                        }
                        if (alt.transcript.Contains("range of projectile") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "The horizontal range of a projectile is the distance along the horizontal plane it would travel, before reaching the same vertical position as it started from.";
                            ResultsField.text = text;
                            //  StartCoroutine(DownloadTheAudio());
                            _audio.clip = Ans14;
                            _audio.Play();
                        }
                        if (alt.transcript.Contains("what is the relation between horizontal range and angle of projection.") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "The horizontal range is equal to product of square of initial velocity and sine of the angle of projection whole divided by the acceleration due to gravity that is g.";
                            ResultsField.text = text;
                          //  StartCoroutine(DownloadTheAudio());
                            _audio.clip = Ans15;
                            _audio.Play();
                        }
                        if (alt.transcript.Contains("relation between horizontal range and angle of projection.") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "The horizontal range is equal to product of square of initial velocity and sine of the angle of projection whole divided by the acceleration due to gravity that is g.";
                            ResultsField.text = text;
                            //  StartCoroutine(DownloadTheAudio());
                            _audio.clip = Ans15;
                            _audio.Play();
                        }
                        if (alt.transcript.Contains("do you want to know what moment is") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "The Moment of a force is a measure of its tendency to cause a body to rotate about a specific point or axis";
                            ResultsField.text = text;
                           // StartCoroutine(DownloadTheAudio());
                            _audio.clip = Ans16;
                            _audio.Play();
                        }
                        if (alt.transcript.Contains("what is the principle of the moments.") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "The principle of moments states that when in equilibrium the total sum of the anti clockwise moment is equal to the total sum of the clockwise moment.";
                            ResultsField.text = text;
                           // StartCoroutine(DownloadTheAudio());
                            _audio.clip = Ans17;
                            _audio.Play();
                        }
                        if (alt.transcript.Contains("principle of the moments.") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "The principle of moments states that when in equilibrium the total sum of the anti clockwise moment is equal to the total sum of the clockwise moment.";
                            ResultsField.text = text;
                            // StartCoroutine(DownloadTheAudio());
                            _audio.clip = Ans17;
                            _audio.Play();
                        }
                        if (alt.transcript.Contains("what is the force of friction.") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "A force of friction is any force that opposes the motion of an object due to the contact of the object with other bodies.";
                            ResultsField.text = text;
                           // StartCoroutine(DownloadTheAudio());
                            _audio.clip = Ans18;
                            _audio.Play();
                        }
                        if (alt.transcript.Contains("force of friction.") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "A force of friction is any force that opposes the motion of an object due to the contact of the object with other bodies.";
                            ResultsField.text = text;
                            // StartCoroutine(DownloadTheAudio());
                            _audio.clip = Ans18;
                            _audio.Play();
                        }
                        if (alt.transcript.Contains("what is sliding friction") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "Sliding friction refers to the resistance created by two objects sliding against each other, also known as kinetic friction. Sliding friction is intended to stop an object from moving.";
                            ResultsField.text = text;
                           // StartCoroutine(DownloadTheAudio());
                            _audio.clip = Ans19;
                            _audio.Play();
                        }
                        if (alt.transcript.Contains("sliding friction") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "Sliding friction refers to the resistance created by two objects sliding against each other, also known as kinetic friction. Sliding friction is intended to stop an object from moving.";
                            ResultsField.text = text;
                            // StartCoroutine(DownloadTheAudio());
                            _audio.clip = Ans19;
                            _audio.Play();
                        }
                        if (alt.transcript.Contains("what is normal reaction force.") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "The force exerted by the ground or other object that prevents other objects from going through it.";
                            ResultsField.text = text;
                           // StartCoroutine(DownloadTheAudio());
                            _audio.clip = Ans20;
                            _audio.Play();
                        }
                        if (alt.transcript.Contains("normal reaction force.") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "The force exerted by the ground or other object that prevents other objects from going through it.";
                            ResultsField.text = text;
                            // StartCoroutine(DownloadTheAudio());
                            _audio.clip = Ans20;
                            _audio.Play();
                        }
                        if (alt.transcript.Contains("normal reaction") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "The force exerted by the ground or other object that prevents other objects from going through it.";
                            ResultsField.text = text;
                            // StartCoroutine(DownloadTheAudio());
                            _audio.clip = Ans20;
                            _audio.Play();
                        }
                        if (alt.transcript.Contains("what is the coefficient of friction.") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "A coefficient of friction is a value that shows the relationship between two objects and the normal reaction between the objects that are involved.";
                            ResultsField.text = text;
                          //  StartCoroutine(DownloadTheAudio());
                            _audio.clip = Ans21;
                            _audio.Play();
                        }
                        if (alt.transcript.Contains("coefficient of friction.") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "A coefficient of friction is a value that shows the relationship between two objects and the normal reaction between the objects that are involved.";
                            ResultsField.text = text;
                            //  StartCoroutine(DownloadTheAudio());
                            _audio.clip = Ans21;
                            _audio.Play();
                        }
                        if (alt.transcript.Contains("what is the coefficient of limiting friction.") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "Coefficient of limiting friction is the coefficient of friction for the two body just before it starts moving";
                            ResultsField.text = text;
                          // StartCoroutine(DownloadTheAudio());
                            _audio.clip = Ans22;
                            _audio.Play();
                        }
                        if (alt.transcript.Contains("coefficient of limiting friction.") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "Coefficient of limiting friction is the coefficient of friction for the two body just before it starts moving";
                            ResultsField.text = text;
                            // StartCoroutine(DownloadTheAudio());
                            _audio.clip = Ans22;
                            _audio.Play();
                        }
                        if (alt.transcript.Contains("what is the collision ") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "A collision is an event in which two or more bodies exert forces on each other in about a relatively short time.";
                            ResultsField.text = text;
                           // StartCoroutine(DownloadTheAudio());
                            _audio.clip = Ans23;
                            _audio.Play();
                        }
                        if (alt.transcript.Contains("collision ") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "A collision is an event in which two or more bodies exert forces on each other in about a relatively short time.";
                            ResultsField.text = text;
                            // StartCoroutine(DownloadTheAudio());
                            _audio.clip = Ans23;
                            _audio.Play();
                        }
                        if (alt.transcript.Contains("what is the momentum of an object ") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "Momentum is the product of the mass and velocity of an object. It is a vector quantity, possessing a magnitude and a direction.";
                            ResultsField.text = text;
                          //  StartCoroutine(DownloadTheAudio());
                            _audio.clip = Ans24;
                            _audio.Play();
                        }
                        if (alt.transcript.Contains("momentum of an object ") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "Momentum is the product of the mass and velocity of an object. It is a vector quantity, possessing a magnitude and a direction.";
                            ResultsField.text = text;
                            //  StartCoroutine(DownloadTheAudio());
                            _audio.clip = Ans24;
                            _audio.Play();
                        }
                        if (alt.transcript.Contains("momentum of object ") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "Momentum is the product of the mass and velocity of an object. It is a vector quantity, possessing a magnitude and a direction.";
                            ResultsField.text = text;
                            //  StartCoroutine(DownloadTheAudio());
                            _audio.clip = Ans24;
                            _audio.Play();
                        }
                        if (alt.transcript.Contains("what is momentum ") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "Momentum is the product of the mass and velocity of an object. It is a vector quantity, possessing a magnitude and a direction.";
                            ResultsField.text = text;
                            //  StartCoroutine(DownloadTheAudio());
                            _audio.clip = Ans24;
                            _audio.Play();
                        }
                        if (alt.transcript.Contains("momentum ") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "Momentum is the product of the mass and velocity of an object. It is a vector quantity, possessing a magnitude and a direction.";
                            ResultsField.text = text;
                            //  StartCoroutine(DownloadTheAudio());
                            _audio.clip = Ans24;
                            _audio.Play();
                        }
                        if (alt.transcript.Contains("what are the types of collision.") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "There are two types of collisions, elastic collision and inelastic collision";
                            ResultsField.text = text;
                         //   StartCoroutine(DownloadTheAudio());
                            _audio.clip = Ans25;
                            _audio.Play();
                        }
                        if (alt.transcript.Contains("types of collision.") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "There are two types of collisions, elastic collision and inelastic collision";
                            ResultsField.text = text;
                            //   StartCoroutine(DownloadTheAudio());
                            _audio.clip = Ans25;
                            _audio.Play();
                        }
                        if (alt.transcript.Contains("collision type.") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "There are two types of collisions, elastic collision and inelastic collision";
                            ResultsField.text = text;
                            //   StartCoroutine(DownloadTheAudio());
                            _audio.clip = Ans25;
                            _audio.Play();
                        }

                        if (alt.transcript.Contains("by") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "byeee";
                            ResultsField.text = text;
                         //   StartCoroutine(DownloadTheAudio());
                            _audio.clip = Ans26;
                            _audio.Play();
                        }
                        if (alt.transcript.Contains("bye") && ResultsField.text.Contains("Final")) // needs to be final or ECHO happens
                        {
                            text = "byeee";
                            ResultsField.text = text;
                            //   StartCoroutine(DownloadTheAudio());
                            _audio.clip = Ans26;
                            _audio.Play();
                        }







                        /*
                                               if (!alt.transcript.Contains("bye") && ResultsField.text.Contains("Final"))

                                                                                { 
                                                                                     text = "I am soo Sorry, can you ask again";
                                                                                     ResultsField.text = text;
                                                                                     StartCoroutine(DownloadTheAudio());
                                                                                }
                        */
                    }

                    if (res.keywords_result != null && res.keywords_result.keyword != null)
                    {
                        foreach (var keyword in res.keywords_result.keyword)
                        {
                            Log.Debug("ExampleStreaming.OnRecognize()", "keyword: {0}, confidence: {1}, start time: {2}, end time: {3}", keyword.normalized_text, keyword.confidence, keyword.start_time, keyword.end_time);
                        }
                    }

                    if (res.word_alternatives != null)
                    {
                        foreach (var wordAlternative in res.word_alternatives)
                        {
                            Log.Debug("ExampleStreaming.OnRecognize()", "Word alternatives found. Start time: {0} | EndTime: {1}", wordAlternative.start_time, wordAlternative.end_time);
                            foreach (var alternative in wordAlternative.alternatives)
                                Log.Debug("ExampleStreaming.OnRecognize()", "\t word: {0} | confidence: {1}", alternative.word, alternative.confidence);
                        }
                    }
                }
            }
        }

        private void OnRecognizeSpeaker(SpeakerRecognitionEvent result)
        {
            if (result != null)
            {
                foreach (SpeakerLabelsResult labelResult in result.speaker_labels)
                {
                    Log.Debug("ExampleStreaming.OnRecognizeSpeaker()", string.Format("speaker result: {0} | confidence: {3} | from: {1} | to: {2}", labelResult.speaker, labelResult.from, labelResult.to, labelResult.confidence));
                }
            }
        }

       
       

      

    }
}